﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Dictionar
{
    /// <summary>
    /// Interaction logic for AddPage.xaml
    /// </summary>
    public partial class AddPage : Page
    {
        public AddPage()
        {
            InitializeComponent();
            Dictionary.cuvinte.Clear();
            File.ReadFile();
            Dictionary.AddListCatgorii();
        }

        private void btn_add_Click(object sender, RoutedEventArgs e)
        {
            if (categorieBox.SelectedItem == null)
            {
                Dictionary.cuvinte.Clear();
                File.ReadFile();

                Cuvant cuvant = new Cuvant();
                cuvant.Categorie = altaCategorieBox.Text.ToString();
                cuvant.Cuvantul = cuvantBox.Text;
                cuvant.Descriere = descriereBox.Text;
                //
                cuvant.ImgPath = imagebox.Source.ToString();

                Dictionary.cuvinte.Add(cuvant);
                Dictionary.AddListCatgorii();

                File.WriteList();

                 altaCategorieBox.Text = "";
                 cuvantBox.Text = "";
                 descriereBox.Text = "";
                imagebox.Visibility = Visibility.Hidden;
                 
              

            }
            else
            {
                Dictionary.cuvinte.Clear();
                File.ReadFile();

                Cuvant cuvant = new Cuvant();
                cuvant.Categorie = categorieBox.SelectedItem.ToString();
                cuvant.Cuvantul = cuvantBox.Text;
                cuvant.Descriere = descriereBox.Text;
                //
                cuvant.ImgPath = imagebox.Source.ToString();

                Dictionary.cuvinte.Add(cuvant);
                File.WriteList();

                altaCategorieBox.Text = "";
                cuvantBox.Text = "";
                descriereBox.Text = "";
                imagebox.Visibility = Visibility.Hidden;
            }
           


        }

        private void addImageBtn_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image files (*.png)|*.png|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
                imagebox.Source = new BitmapImage(new Uri(openFileDialog.FileName, UriKind.Absolute));
            imagebox.Visibility = Visibility.Visible;
        }
    }
}
