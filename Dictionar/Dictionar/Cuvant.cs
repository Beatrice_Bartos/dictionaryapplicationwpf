﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dictionar
{
    class Cuvant
    {
        private string cuvant;
        private string descriere;
        private string categorie;
        private string imgPath;

        public Cuvant()
        {
            cuvant = "";
            descriere = "";
            categorie = "";
            imgPath = "";
        }
        
        public string Cuvantul
        {
            get { return cuvant; }
            set { cuvant = value; }
        }

        public string Categorie
        {

            get { return categorie; }
            set { categorie = value; }
        }

        public string Descriere
        {

            get { return descriere; }
            set { descriere = value; }
        }
        public string ImgPath
        {
            get { return imgPath; }
            set { imgPath = value; }
        }
    }
}
