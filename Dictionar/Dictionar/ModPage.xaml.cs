﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Dictionar
{
    /// <summary>
    /// Interaction logic for ModPage.xaml
    /// </summary>
    public partial class ModPage : Page
    {
        
        public ModPage()
        {
            InitializeComponent();
        }

        private void changeBtn_Click(object sender, RoutedEventArgs e)
        {
            string cv = cuvantCerutBox.Text.ToString();

            foreach (var cuv in Dictionary.cuvinte.Where(w => w.Cuvantul == cv))
            {
                if (String.IsNullOrEmpty(categorieModificataBox.Text))
                {
                    cuv.Descriere = descriereModificataBox.Text;
                }
                
                if (String.IsNullOrEmpty(descriereModificataBox.Text))
                {
                    cuv.Categorie = categorieModificataBox.Text;
                }
               
                if ((categorieModificataBox.Text.Length > 0)&& (descriereModificataBox.Text.Length > 0))
                {
                    cuv.Descriere = descriereModificataBox.Text;
                    cuv.Categorie = categorieModificataBox.Text;
                }
               
              
            }
           
            File.WriteList();
            cuvantCerutBox.Text = "";
            categorieModificataBox.Text = "";
            descriereModificataBox.Text = "";


        }
    }
}
