﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Dictionar
{
    /// <summary>
    /// Interaction logic for DeletePage.xaml
    /// </summary>
    public partial class DeletePage : Page
    {
        public DeletePage()
        {
            InitializeComponent();
        }

        private void deleteBtn_Click(object sender, RoutedEventArgs e)
        {
            string cv = deleteCuvantBox.Text.ToString();
            foreach (Cuvant cuv in Dictionary.cuvinte)
            {
                if(cuv.Cuvantul== cv)
                {
                    Dictionary.cuvinte.Remove(cuv);
                    File.WriteList();
                    break;
                }
            }
            deleteCuvantBox.Text = "";
        }
    }
}
