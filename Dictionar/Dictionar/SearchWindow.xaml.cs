﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Dictionar
{
    /// <summary>
    /// Interaction logic for SearchWindow.xaml
    /// </summary>
    public partial class SearchWindow : Window
    {
        public static List<string> list= new List<string>();
        public SearchWindow()
        {
            InitializeComponent();
            Dictionary.cuvinte.Clear();
            File.ReadFile();
            Dictionary.AddListCatgorii();
            method();
           
            
        }
        // <TextBox  Name="cuvantBoxSearch" HorizontalAlignment="Left" Height="40" Margin="192,135,0,0" TextWrapping="Wrap" Text="" VerticalAlignment="Top" Width="197" Background="Beige"/>

        private void searchBtn_Click(object sender, RoutedEventArgs e)
        {
            foreach (Cuvant cuv in Dictionary.cuvinte)
            {
                if (cuv.Cuvantul== textBox.Text)
                {
                    labelDescriere.Content = cuv.Descriere;
                    img.Source = new BitmapImage(new Uri(cuv.ImgPath));
                    img.Visibility = Visibility.Visible;
                }
            }

            
        }
        private static string cboParser(string controlString)
        {
            if (controlString.Contains(':'))
            {
                controlString = controlString.Split(':')[1].TrimStart(' ');
            }
            return controlString;
        }
        
        public void method()
        {
            //list.Clear();

            if (categorieBoxSearch.SelectedItem!=null)
            {
                string categorie = categorieBoxSearch.SelectedItem.ToString();
                //string yourValue = cboParser(categorieBoxSearch.SelectedItem.ToString());
                foreach (Cuvant cuv in Dictionary.cuvinte)
                {
                    if (cuv.Categorie == categorie)
                    {
                        list.Add(cuv.Cuvantul);
                    }
                }
            }
            else
            if (categorieBoxSearch.SelectedItem == null)
            {
                foreach (Cuvant cuv in Dictionary.cuvinte)
                {
                    list.Add(cuv.Cuvantul);
                }
            }
            
            
            
        }
         static public List<string> GetData()
        {
            return list;
        }

        private void addItem(string text)
        {
            TextBlock block = new TextBlock();

            // Add the text   
            block.Text = text;

            // A little style...   
            block.Margin = new Thickness(2, 3, 2, 3);
            block.Cursor = Cursors.Hand;

            // Mouse events   
            block.MouseLeftButtonUp += (sender, e) =>
            {
                textBox.Text = (sender as TextBlock).Text;
            };

            block.MouseEnter += (sender, e) =>
            {
                TextBlock b = sender as TextBlock;
                b.Background = Brushes.PeachPuff;
            };

            block.MouseLeave += (sender, e) =>
            {
                TextBlock b = sender as TextBlock;
                b.Background = Brushes.Transparent;
            };

            // Add to the panel   
            resultStack.Children.Add(block);
        }
        private void textBox_KeyUp(object sender, KeyEventArgs e)
        {
            bool found = false;
            var border = (resultStack.Parent as ScrollViewer).Parent as Border;
            var data = GetData();

            string query = (sender as TextBox).Text;

            if (query.Length == 0)
            {
                // Clear   
                resultStack.Children.Clear();
                border.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                border.Visibility = System.Windows.Visibility.Visible;
            }

            // Clear the list   
            resultStack.Children.Clear();

            // Add the result   
            foreach (var obj in data)
            {
                if (obj.ToLower().StartsWith(query.ToLower()))
                {
                    // The word starts with this... Autocomplete must work   
                    addItem(obj);
                    found = true;
                }
            }

            if (!found)
            {
                resultStack.Children.Add(new TextBlock() { Text = "No results found." });
            }
        }
    }
}
