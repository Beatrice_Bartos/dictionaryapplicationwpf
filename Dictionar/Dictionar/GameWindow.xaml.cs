﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Dictionar
{
    /// <summary>
    /// Interaction logic for GameWindow.xaml
    /// </summary>
    public partial class GameWindow : Window
    {
        public int contor = 0;
        public int raspunsuriCorecte = 1;
        public int nrIntrebare = 1;
        public int RaspunsuriCorecte
        {
            set
            {
                raspunsuriCorecte = value;
            }
            get
            {
                return raspunsuriCorecte;
            }
        }
        public int Contor
        {
            set
            {
                contor = value;
            }
            get
            {
                return contor;
            }
        }
        public GameWindow()
        {
            InitializeComponent();
            methodRand();
        }

        public void methodRand()
        {



            Random rand = new Random();
            List<string> descrieri = new List<string>();
            string[] fisier= System.IO.File.ReadAllLines("test.txt");
            for (int i = 0; i < fisier.Length - 3; i =i+4)
            {
                descrieri.Add(fisier[i + 2]);
            }
            int pozitieRandom = rand.Next(descrieri.Count());
            intrebare.Text =descrieri[pozitieRandom];
            nrIntrebare++;

        }

       public void Restart()
        {
            this.Close();
            GameWindow window = new GameWindow();
            window.Show();
        }
        private void raspundBtn_Click(object sender, RoutedEventArgs e)
        {
            Restart();
        }

        private void checkBtn_Click(object sender, RoutedEventArgs e)
        {
           
            string cuvant, descriere;
            cuvant = raspunsBox.Text;
            descriere = intrebare.Text;
       
            string[] fisier = System.IO.File.ReadAllLines("test.txt");
            int ok = 0;
            for (int i = 0; i < fisier.Length - 3; i += 4)
            {
                if (cuvant == fisier[i+1] && descriere == fisier[i + 2])
                {
                    if (contor <4)
                    {
                        contor++;
                        ok = 1;
                      
                        label.Content=contor+": Raspuns Corect!";
                        raspunsuriCorecte++;
                    }
                    else
                    {
                        if (raspunsuriCorecte == 5)
                        {
                             MessageBox.Show("Finalul Jocului!\nFelicitari! Ati raspuns corect la toate intrebarile");
                            
                            Restart();
                            break;
                        }
                        else
                        {
                            MessageBox.Show("Finalul Jocului!\nAti raspuns corect la " + raspunsuriCorecte + " din 5 ");
                           
                            Restart();
                            break;
                        }
                    }
                }
            }
            if (ok == 0 && contor < 4)
            {
                contor++;
                label.Content = contor + ": Raspuns gresit!";
            }
            
            raspunsBox.Text="";
            intrebare.Text="";
            methodRand();
            
        }
    }
}
