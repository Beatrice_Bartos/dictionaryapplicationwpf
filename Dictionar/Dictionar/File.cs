﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dictionar
{
    class File
    {
        public static void WriteList()
        {
           
            StreamWriter sw = new StreamWriter("test.txt");
            sw.Write(string.Empty);
            foreach (Cuvant cuvant in Dictionary.cuvinte)
            {
                sw.WriteLine(cuvant.Categorie);
                sw.WriteLine(cuvant.Cuvantul);
                sw.WriteLine(cuvant.Descriere);
                sw.WriteLine(cuvant.ImgPath);
              
            }
            sw.Close();
        }
        public static void WriteTest(string str)
        {

            StreamWriter sw = new StreamWriter("dateTest.txt");
            sw.Write(string.Empty);
            foreach (Cuvant cuvant in Dictionary.cuvinte)
            {
                if(cuvant.Cuvantul==str)
                sw.WriteLine(cuvant.ImgPath);

            }
            sw.Close();
        }

        public static void ReadFile()
        {
            string[] lines = System.IO.File.ReadAllLines("test.txt");
           
            for(int i=0;i<lines.Length;i=i+4)
            {
                Dictionary.cuvinte.Add(new Cuvant() { Categorie = lines[i], Cuvantul = lines[i+1], Descriere = lines[i+2], ImgPath=lines[i+3]});
            }

        }

      
    }
}
