﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Dictionar
{
    /// <summary>
    /// Interaction logic for AdminWindow.xaml
    /// </summary>
    public partial class AdminWindow : Window
    {
        public AdminWindow()
        {
            InitializeComponent();
        }

        private void addBtn_Click(object sender, RoutedEventArgs e)
        {
            AdminFrame.Content = new AddPage();
        }

        private void modBtn_Click(object sender, RoutedEventArgs e)
        {
            AdminFrame.Content = new ModPage();
        }

        private void deleteBtn_Click(object sender, RoutedEventArgs e)
        {
            AdminFrame.Content = new DeletePage();
        }
    }
}
