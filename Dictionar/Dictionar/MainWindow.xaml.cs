﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Dictionar
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

      
        private void adminButton_Click(object sender, RoutedEventArgs e)
        {
            
                adminButton.FontSize = 16;
                adminButton.Content = "Admin Module";
                adminButton.Background = Brushes.Purple;

               AdminWindow adm = new AdminWindow();
               adm.Show();

        }

   
        private void gameButton_Click(object sender, RoutedEventArgs e)
        {
            
                gameButton.FontSize = 16;
                gameButton.Content = "Game Module";
                gameButton.Background = Brushes.Purple;

            GameWindow game= new GameWindow();
            game.Show();

        }

        private void searchButton_Click_1(object sender, RoutedEventArgs e)
        {
                searchButton.FontSize = 16;
                searchButton.Content = "Search Module";
                searchButton.Background = Brushes.Purple;

            SearchWindow search = new SearchWindow();
            search.Show();
        }
    }
}
