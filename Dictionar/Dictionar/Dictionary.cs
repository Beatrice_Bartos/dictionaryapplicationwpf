﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dictionar
{
    class Dictionary
    {
        public static Cuvant cuvant { get; set; }
        public  static List<Cuvant> cuvinte = new List<Cuvant>();
        public static HashSet<String> categorii { get; set; } = new HashSet<string>();
        
        public Dictionary()
        {
            cuvant = new Cuvant();
            cuvinte = new List<Cuvant>();
            categorii = new HashSet<string>();
            
        }
       
       
        public static void AddListCatgorii()
        {
            categorii.Clear();
            foreach(Cuvant cuv in cuvinte)
            {
                categorii.Add(cuv.Categorie);
            }
        }
    }
}
